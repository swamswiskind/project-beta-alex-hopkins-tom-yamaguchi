from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=100)
    sold = models.BooleanField()

    def __str__(self):
        return f"{self.vin}"


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.AutoField(primary_key=True, unique=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Appointment(models.Model):
    date_time = models.DateTimeField(auto_now=False, auto_now_add=False)
    reason = models.TextField()
    vin = models.CharField(max_length=100)
    customer_href = models.CharField(max_length=100)
    customer = models.CharField(max_length=100)
    status = models.CharField(max_length=100)
    technician = models.ForeignKey(
        Technician,
        on_delete=models.SET_NULL,
        null=True
    )

    def __str__(self):
        return f"{self.vin} {self.status}"