from django.views.decorators.http import require_http_methods
from django.http import JsonResponse, Http404, HttpResponse
from django.shortcuts import get_object_or_404
from common.json import ModelEncoder
import json
import datetime
from .models import Technician, Appointment, AutomobileVO


class TechEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id"
    ]


class AppEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "reason",
        "vin",
        "customer_href",
        "customer",
        "status",
    ]

    def get_extra_data(self, o):
        return {
            "technician": (f"{o.technician.first_name} {o.technician.last_name}" if hasattr(o.technician, "first_name") else "Fired"),
            "date": o.date_time.date(),
            "time": str(o.date_time.time()),
            "vip": ("Yes" if AutomobileVO.objects.filter(vin=o.vin).exists() else "No")
                }



@require_http_methods(["GET", "POST"])
def create_list_techs(request):
    if request.method == "GET":
        try:
            tech = Technician.objects.all()
            return JsonResponse({"Technicians": tech}, encoder=TechEncoder, safe=False)
        except Exception as err:
            return JsonResponse({"message": str(err)}, status=400)

    else:
        try:
            response = json.loads(request.body)
            tech = Technician.objects.create(**response)

            return JsonResponse(tech, encoder=TechEncoder, safe=False)

        except Exception as err:
            return JsonResponse({"message": str(err)}, status=404)


@require_http_methods(["DELETE"])
def delete_tech(request, id):
    count, _ = get_object_or_404(Technician, employee_id=id).delete()
    return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def create_list_appointment(request):
    if request.method == "GET":
        try:
            app = Appointment.objects.all()
            return JsonResponse({"Appointments": app}, encoder=AppEncoder, safe=False)
        except Exception as err:
            return JsonResponse({"message": str(err)}, status=400)

    else:
        try:
            response = json.loads(request.body)
            tech = Technician.objects.get(employee_id=response["technician"])
            response["technician"] = tech
            response["status"] = "Pending"
            d = datetime.datetime.fromisoformat(response["date_time"])
            response["date_time"] = d
            app = Appointment.objects.create(**response)

            return JsonResponse(app, encoder=AppEncoder, safe=False)

        except Exception as err:
            return JsonResponse({"message": str(err)}, status=404)


@require_http_methods(["DELETE"])
def delete_appointment(request, id):
    count, _ = get_object_or_404(Appointment, id=id).delete()
    return JsonResponse({"deleted": count > 0}, status=200)


@require_http_methods(["PUT"])
def set_canceled(request, id):
    status = Appointment.objects.filter(id=id).update(status="Canceled")

    if status is 0:
        return JsonResponse({"Error": "Appointment Object Does Not Exist"}, status=404)
    else:
        return JsonResponse({"Success": "Status Updated"})


@require_http_methods(["PUT"])
def set_finished(request, id):
    status = Appointment.objects.filter(id=id).update(status="Finished")

    if status is 0:
        return JsonResponse({"Error": "Appointment Object Does Not Exist"}, status=404)
    else:
        return JsonResponse({"Success": "Status Updated"})