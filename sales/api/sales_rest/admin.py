from django.contrib import admin
from .models import Salesperson, Customer, Sale, AutomobileVO

admin.site.register(Salesperson)
class SalespersonAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'employee_id')
    search_fields = ('first_name', 'last_name', 'employee_id')

admin.site.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'address', 'phone_number')
    search_fields = ('first_name', 'last_name', 'address', 'phone_number')

admin.site.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    list_display = ('automobile', 'salesperson', 'customer', 'price')
    search_fields = ('automobile', 'salesperson', 'customer', 'price')


admin.site.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    list_display = ('vin', 'sold')
    search_fields = ('vin', 'sold')
