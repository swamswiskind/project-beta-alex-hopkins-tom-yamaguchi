import django
import os
import sys
import time
import json
import requests

# Configure Django settings
sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

from sales_rest.models import AutomobileVO

def poll():
    while True:
        print('Sales poller polling for data')
        try:
            response = requests.get("http://inventory-api:8000/api/automobiles/")
            content = json.loads(response.content)
            for auto in content["autos"]:
                AutomobileVO.objects.update_or_create(
                    import_href=auto["href"],
                    defaults={
                        "vin": auto["vin"],
                        "sold": bool(auto["sold"])
                    }
                )
        except Exception as e:
            print("Sales poller error occurred.")
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
# Compare this snippet from sales/api/sales_rest/views.py:
