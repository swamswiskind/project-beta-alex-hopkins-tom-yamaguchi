import { useState, useEffect } from "react";
import { Link } from 'react-router-dom';

export default function ListCustomers() {
    const [customers, setCustomers] = useState([]);

    const fetchCustomers = async () => {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    };

    const deleteCustomer = async (event, id) => {
        event.preventDefault();
        const url = `http://localhost:8090/api/customers/${id}`;
        const response = await fetch(url, { method: 'DELETE' });
        if (response.ok) {
            setCustomers(customers.filter(customer => customer.id !== id));
        }
    };

    useEffect(() => {
        fetchCustomers();
    }, []);

    return (
        <div className='container'>
            <div className="d-grid gap-2 d-sm-flex justify-content-between" style={{ margin: "1rem" }}>
                <Link to="new" className="btn btn-primary">
                    Create a new customer
                </Link>
            </div>

            <div className="col-12">
                <h2>Customers</h2>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Phone Number</th>
                            <th>Address</th>
                        </tr>
                    </thead>
                    <tbody>
                        {customers.map((customer, index) => (
                            <tr
                                key={customer.id}
                                style={index % 2 === 0 ? { backgroundColor: 'lightgrey' } : null}
                            >
                                <td>{customer.id}</td>
                                <td>{customer.first_name}</td>
                                <td>{customer.last_name}</td>
                                <td>{customer.phone_number}</td>
                                <td>{customer.address}</td>
                                <td>
                                    <button className="btn btn-danger" onClick={(event) => deleteCustomer(event, customer.id)}>Delete</button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
}
