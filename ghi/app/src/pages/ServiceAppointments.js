import { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
import { DetailTable } from "../components/DetailTable";

export default function ListServiceAppointments() {
    const [details, setDetails] = useState([])
    const includedValues = ["vin", "vip", "customer", "date", "time", "technician", "reason"]
    const buttons = [{"name": "Cancel", "class": "btn btn-danger me-1", "click": handleCancel}, {"name": "Finished", "class": "btn btn-success me-1", "click": handleFinished}]

    async function handleCancel(event) {
        const id = event.target.value
        const fetchConfig = {
            method: "put",
            headers: {
                "Content-Type": 'application/json',
            }
        }
        const response = await fetch(`http://localhost:8080/api/appointments/${event.target.value}/cancel/`, fetchConfig)
        if (response.ok) {
            const copyData = [...details]
            setDetails(copyData.filter((detail) => detail.id != id))
        }
    }

    async function handleFinished(event) {
        const id = event.target.value
        const fetchConfig = {
            method: "put",
            headers: {
                "Content-Type": 'application/json',
            }
        }
        const response = await fetch(`http://localhost:8080/api/appointments/${event.target.value}/finish/`, fetchConfig)
        if (response.ok) {
            const copyData = [...details]
            setDetails(copyData.filter((detail) => detail.id != id))
        }
    }

    const fetchData = async () => {
        const response = await fetch("http://localhost:8080/api/appointments/")
        if (response.ok) {
            const data = await response.json()
            setDetails(data.Appointments.filter(item => item.status == "Pending"))
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className="container">
            <div className="d-grid gap-2 d-sm-flex justify-content-between" style={{margin: "1rem"}}>
                <h3>Appointments:</h3>
                <Link to="new" className="btn btn-success btn-lg px-4 gap-3">Add an Appointment</Link>
            </div>
            <hr/>
            {details.length > 0 ? DetailTable(details, includedValues, buttons, "id") : null}
        </div>
    )
}
