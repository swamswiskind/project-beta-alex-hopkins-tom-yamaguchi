import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

export default function VehicleModelList() {
  const [vehicleModels, setVehicleModels] = useState([]);

  useEffect(() => {
    const fetchVehicleModels = async () => {
      const response = await fetch('http://localhost:8100/api/models/');
      const data = await response.json();
      setVehicleModels(data.models);
    };

    fetchVehicleModels();
  }, []);

  const deleteModel = async (event, id) => {
    event.preventDefault();
    const url = `http://localhost:8100/api/models/${id}`;
    const response = await fetch(url, { method: 'DELETE' });
    if (response.ok) {
      setVehicleModels(vehicleModels.filter((model) => model.id !== id));
    }
  };

  return (
    <div className="container">
      <div className="d-grid gap-2 d-sm-flex justify-content-between" style={{ margin: '1rem' }}>
        <Link to="new" className="btn btn-primary">
          Create a new customer
        </Link>
      </div>

      <div className="col-12">
        <h2>Vehicle Models</h2>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Manufacturer</th>
              <th>Picture</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {vehicleModels.map((vehicleModel, index) => (
              <tr
                key={vehicleModel.id}
                style={index % 2 === 0 ? { backgroundColor: 'lightgrey' } : null}
              >
                <td>{vehicleModel.name}</td>
                <td>{vehicleModel.manufacturer.name}</td>
                <td>
                  <img
                    src={vehicleModel.picture_url}
                    alt={vehicleModel.name}
                    style={{ borderRadius: '20px' }}
                    width="200"
                    height="100"
                  />
                </td>
                <td>
                  <button className="btn btn-danger" onClick={(event) => deleteModel(event, vehicleModel.id)}>
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}
