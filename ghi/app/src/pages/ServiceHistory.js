import { useState, useEffect } from "react";
import { DetailTable } from "../components/DetailTable";

export default function ServiceHistory() {
    const [details, setDetails] = useState([])
    const [vins, setVins] = useState([])
    const includedValues = ["vin", "vip", "customer", "date", "time", "technician", "reason", "status"]
    const [search, setSearch] = useState("")

    const handleChange = (event) => {
        setSearch(event.target.value.toUpperCase())
    }

    const fetchData = async () => {
        const response = await fetch("http://localhost:8080/api/appointments/")
        if (response.ok) {
            const data = await response.json()
            setDetails(data.Appointments)
            setVins([...new Set(data.Appointments.map(item => item.vin))])

        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className="container">
            <div className="d-flex gap-2 flex-column justify-content-between" id="test" style={{margin: "1rem"}}>
                <h3>Service History:</h3>
                <div className="search-bar" id="search-bar">
                    <input
                        value={search}
                        onChange={handleChange}
                        placeholder="Search VIN..."
                        type="text"
                        name="search"
                        id="search"
                        className="form-control"
                        data-bs-toggle="dropdown"
                        autoComplete="off"
                        />
                    <button type="button" className="btn btn-close" id="close-button" onClick={() => setSearch("")}></button>
                    <ul className="dropdown-menu"  aria-labelledby="search">
                        {vins.filter(item => item.startsWith(search.toUpperCase())).map(vin => {
                            return (
                                <li key={vin}><button className="dropdown-item" style={{position: "relative"}} value={vin} onClick={handleChange}> {vin} </button></li>
                            )
                        })}
                    </ul>
                </div>
            </div>
            <hr/>
            {details.length > 0 ? DetailTable(details.filter(item => item.vin.startsWith(search.toUpperCase())), includedValues) : null}
        </div>
    )
}
