import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

export default function SalesList() {
    const [salesList, setSalesList] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8090/api/sales/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSalesList(data);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const deleteSale = async (event, id) => {
        event.preventDefault();
        const url = `http://localhost:8090/api/sales/${id}`;
        const response = await fetch(url, { method: "DELETE" });

        if (response.ok) {
            setSalesList(salesList.filter((sale) => sale.id !== id));
        }
    };

    return (
        <div className='container'>
            <div className="d-grid gap-2 d-sm-flex justify-content-between" style={{ margin: "1rem" }}>
                <Link to="new" className="btn btn-primary">
                    Create a Sale
                </Link>
            </div>

            <div className="col-12">
                <h2>Sales</h2>
            </div>

            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Employee ID</th>
                        <th>Salesperson Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {salesList.sales?.map((sale, index) => {
                        return (
                            <tr
                                key={sale.id}
                                style={index % 2 === 0 ? { backgroundColor: 'lightgrey' } : null}
                            >
                                <td>{sale.salesperson.employee_id}</td>
                                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>${sale.price}.00</td>
                                <td>
                                    <button onClick={(event) => deleteSale(event, sale.id)} className="btn btn-danger btn-sm">Delete</button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    );
}
