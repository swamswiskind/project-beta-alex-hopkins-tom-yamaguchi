import React, { useState, useEffect } from 'react';

export default function SaleForm() {
  const [vin, setVin] = useState('');
  const [salesperson, setSalesperson] = useState('');
  const [customer, setCustomer] = useState('');
  const [price, setPrice] = useState('');
  const [autos, setAutos] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [customers, setCustomers] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    const autosUrl = "http://localhost:8100/api/automobiles/";
    const autosResponse = await fetch(autosUrl);
    if (autosResponse.ok) {
      const autosData = await autosResponse.json();
      const autosAvailable = autosData.autos.filter(auto => !auto.sold);
      setAutos(autosAvailable);
    }

    const salespeopleUrl = "http://localhost:8090/api/salespeople/";
    const salespeopleResponse = await fetch(salespeopleUrl);
    if (salespeopleResponse.ok) {
      const salespeopleData = await salespeopleResponse.json();
      setSalespeople(salespeopleData.salespeople);
    }

    const customersUrl = "http://localhost:8090/api/customers/";
    const customersResponse = await fetch(customersUrl);
    if (customersResponse.ok) {
      const customersData = await customersResponse.json();
      setCustomers(customersData.customers);
    }
  };

  const handleSubmit = async event => {
    event.preventDefault();

    const data = {
      automobile: vin,
      salesperson: salesperson,
      customer: customer,
      price: price
    };

    const url = "http://localhost:8090/api/sales/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json"
      }
    };

    const soldUrl = `http://localhost:8100/api/automobiles/${vin}/`;
    const fetchSoldConfig = {
      method: "put",
      body: JSON.stringify({ sold: true }),
      headers: {
        "Content-Type": "application/json"
      }
    };

    const response = await fetch(url, fetchConfig);
    const soldResponse = await fetch(soldUrl, fetchSoldConfig);

    if (response.ok && soldResponse.ok) {
      const autoUrl = "http://localhost:8100/api/automobiles/";
      fetch(autoUrl);
      setVin('');
      setSalesperson('');
      setCustomer('');
      setPrice('');
    }
  };

  const handleVinChange = event => {
    const value = event.target.value;
    setVin(value);
  };

  const handleSalespersonChange = event => {
    const value = event.target.value;
    setSalesperson(value);
  };

  const handleCustomerChange = event => {
    const value = event.target.value;
    setCustomer(value);
  };

  const handlePriceChange = event => {
    const value = event.target.value;
    setPrice(value);
  };

  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-8">
          <div className="shadow p-4 mt-4">
            <h1 className="text-center mb-4">Add a Sale</h1>
            <form onSubmit={handleSubmit} id="create-sales-form">
              <div className="mb-3">
                <label htmlFor="vin" className="form-label">
                  Choose an automobile VIN
                </label>
                <select
                  onChange={handleVinChange}
                  value={vin}
                  required
                  id="vin"
                  className="form-select"
                >
                  <option value="">Choose an automobile VIN</option>
                  {autos.map(vin => (
                    <option key={vin.vin} value={vin.vin}>
                      {vin.model.name} / VIN: {vin.vin}
                    </option>
                  ))}
                </select>
              </div>
              <div className="mb-3">
                <label htmlFor="salesperson" className="form-label">
                  Choose a salesperson
                </label>
                <select
                  onChange={handleSalespersonChange}
                  value={salesperson}
                  required
                  id="salesperson"
                  className="form-select"
                >
                  <option value="">Choose a salesperson</option>
                  {salespeople.map(salesperson => (
                    <option key={salesperson.employee_id} value={salesperson.employee_id}>
                      {salesperson.first_name} {salesperson.last_name}
                    </option>
                  ))}
                </select>
              </div>
              <div className="mb-3">
                <label htmlFor="customer" className="form-label">
                  Choose a customer
                </label>
                <select
                  onChange={handleCustomerChange}
                  value={customer}
                  required
                  id="customer"
                  className="form-select"
                >
                  <option value="">Choose a customer</option>
                  {customers.map(customer => (
                    <option key={customer.id} value={customer.id}>
                      {customer.first_name} {customer.last_name}
                    </option>
                  ))}
                </select>
              </div>
              <div className="mb-3">
                <label htmlFor="price" className="form-label">
                  Price
                </label>
                <div className="form-floating">
                  <input
                    onChange={handlePriceChange}
                    value={price}
                    placeholder="Price"
                    required
                    type="text"
                    id="price"
                    className="form-control"
                  />
                  <label htmlFor="price">Price</label>
                </div>
              </div>
              <div className="text-center">
                <button type="submit" className="btn btn-primary">
                  Create
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
