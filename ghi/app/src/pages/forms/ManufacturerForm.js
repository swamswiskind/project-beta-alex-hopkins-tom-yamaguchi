import { useDefaultState } from "../../components/hooks";
import { useRef, useEffect } from "react";
import { Link } from 'react-router-dom';
import "./forms.css"


export default function ManufacturerForm() {
    const name = useDefaultState()
    let success = useRef(false)

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {
            "name": name.state,

        }
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": 'application/json',
            }
        }
        const response = await fetch("http://localhost:8100/api/manufacturers/", fetchConfig)
        if (response.ok) {
            name.reset()
            success.current = true
        }
    }

    useEffect(() => {
        success.current = false
    }, [name])


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow mt-4 d-flex flex-column bg-success rounded-3">
                    <h1 className="align-self-center text-white p-1">Add Manufacturer</h1>
                    <form className="p-4 bg-light d-flex flex-column" onSubmit={handleSubmit}>
                        { success.current ?
                            <div className="alert alert-success" role="alert">
                                <strong>Success!</strong> New Manufacturer Added!
                            </div>
                            :
                            null
                        }
                        <div className="form-floating mb-3">
                            <input
                                value={name.state}
                                onChange={name.handleChange}
                                placeholder="Name"
                                required type="text"
                                name="name"
                                id="name"
                                className="form-control"
                                />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="d-flex flex-row justify-content-around">
                            <button className="btn btn-success w-25">Add</button>
                            <Link to="../manufacturers" className="btn btn-secondary w-25">Cancel</Link>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}
