import { useState, useEffect, useRef } from "react";
import { useDefaultState } from "../../components/hooks";
import { Link } from 'react-router-dom';
import "./forms.css"

export default function ServiceAppointmentForm() {
    const vin = useDefaultState()
    const customer = useDefaultState()
    const [customerList, setCustomerList] = useState([])
    const date = useDefaultState()
    const tech = useDefaultState()
    const [techList, setTechList] = useState([])
    const reason = useDefaultState()
    let success = useRef(false)

    const fetchData = async () => {
        const customers = await fetch("http://localhost:8090/api/customers/")
        const techs = await fetch("http://localhost:8080/api/technicians/")
        if (customers.ok && techs.ok) {
            const customerData = await customers.json()
            const techData = await techs.json()
            setCustomerList(customerData.customers)
            setTechList(techData.Technicians)
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const customerName = customerList.filter(item => item.href == customer.state)
        const data = {
            "date_time": date.state,
            "reason": reason.state,
            "vin": vin.state,
            "customer_href": customer.state,
            "customer": `${customerName[0].first_name} ${customerName[0].last_name}`,
            "technician": tech.state
        }
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": 'application/json',
            }
        }
        const response = await fetch("http://localhost:8080/api/appointments/", fetchConfig)
        if (response.ok) {
            date.reset()
            reason.reset()
            vin.reset()
            customer.reset()
            tech.reset()
            success.current = true
        }
    }

    useEffect(() => {
        success.current = false
    }, [vin])

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow mt-4 d-flex flex-column bg-success rounded-3">
                    <h1 className="align-self-center text-white p-1">Schedule Appointment</h1>
                    <form className="p-4 bg-light d-flex flex-column" onSubmit={handleSubmit}>
                        { success.current ?
                            <div className="alert alert-success" role="alert">
                                <strong>Success!</strong> New Appointment Scheduled!
                            </div>
                            :
                            null
                        }
                        <div className="form-floating mb-3">
                            <input
                            value={vin.state}
                            onChange={vin.handleChange}
                            placeholder="VIN"
                            required type="text"
                            name="vin"
                            id="vin"
                            className="form-control"
                            />
                            <label htmlFor="vin">VIN:</label>
                        </div>
                        <div className="mb-3">
                            <select
                                value={customer.state}
                                onChange={customer.handleChange}
                                required name="customers"
                                id="customers"
                                className="form-select">
                            <option key="defaultPres" value="">Select a Customer</option>
                            {customerList.map(data => <option key={data.href} value={data.href}>{data.first_name} {data.last_name}</option>)}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                value={date.state}
                                onChange={date.handleChange}
                                required type="datetime-local"
                                name="datetime"
                                id="datetime"
                                className="form-control"
                                />
                            <label htmlFor="datetime">Due Date & Time:</label>
                        </div>
                        <div className="mb-3">
                            <select
                                value={tech.state}
                                onChange={tech.handleChange}
                                required name="techs"
                                id="techs"
                                className="form-select">
                            <option key="defaultPres" value="">Select a Technician</option>
                            {techList.map(data => <option key={data.employee_id} value={data.employee_id}>{data.first_name} {data.last_name}</option>)}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                value={reason.state}
                                onChange={reason.handleChange}
                                placeholder="Reason"
                                required type="text"
                                name="reason"
                                id="reason"
                                className="form-control"
                                />
                            <label htmlFor="reason">Reason:</label>
                        </div>
                        <div className="d-flex flex-row justify-content-around">
                            <button className="btn btn-success">Schedule Appointment</button>
                            <Link to="../appointments" className="btn btn-secondary w-25">Cancel</Link>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}