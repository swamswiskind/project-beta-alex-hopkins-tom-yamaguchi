import { useState, useEffect, useRef } from "react";
import { useDefaultState } from "../../components/hooks";
import { Link } from 'react-router-dom';
import "./forms.css"

export default function AutomobileForm() {
    const vin = useDefaultState()
    const color = useDefaultState()
    const year = useDefaultState()
    const model = useDefaultState()
    const [models, setModels] = useState([])
    let success = useRef(false)

    const fetchData = async () => {
        const modelData = await fetch("http://localhost:8100/api/models/")

        if (modelData.ok) {
            const response = await modelData.json()
            setModels(response.models)

        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    useEffect(() => {
        success.current = false
    }, [vin])

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {
            "vin": vin.state,
            "color": color.state,
            "year": year.state,
            "model_id": model.state
        }
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": 'application/json',
            }
        }
        const response = await fetch("http://localhost:8100/api/automobiles/", fetchConfig)
        if (response.ok) {
            vin.reset()
            color.reset()
            year.reset()
            model.reset()
            success.current = true
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow mt-4 d-flex flex-column bg-success rounded-3">
                    <h1 className="align-self-center text-white p-1">Add Car to Lot</h1>
                    <form className="p-4 bg-light d-flex flex-column" onSubmit={handleSubmit}>
                        { success.current ?
                            <div className="alert alert-success" role="alert">
                                <strong>Success!</strong> Automobile Added to Lot!
                            </div>
                            :
                            null
                        }
                        <div className="form-floating mb-3">
                            <input
                            value={vin.state.toUpperCase()}
                            onChange={vin.handleChange}
                            placeholder="VIN"
                            required type="text"
                            name="vin"
                            id="vin"
                            className="form-control"
                            />
                            <label htmlFor="vin">VIN:</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                value={color.state}
                                onChange={color.handleChange}
                                placeholder="Color"
                                required type="text"
                                name="color"
                                id="color"
                                className="form-control"
                                />
                            <label htmlFor="color">Color:</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                value={year.state}
                                onChange={year.handleChange}
                                required type="number"
                                min={1000}
                                max={9999}
                                name="year"
                                id="year"
                                className="form-control"
                                autoComplete="off"
                                />
                            <label htmlFor="year">Year:</label>
                        </div>
                        <div className="mb-3">
                            <select
                                value={model.state}
                                onChange={model.handleChange}
                                required name="techs"
                                id="techs"
                                className="form-select">
                            <option key="defaultPres" value="">Select a Model</option>
                            {models.map(data => <option key={data.href} value={data.id}>{data.name}</option>)}
                            </select>
                        </div>
                        <div className="d-flex flex-row justify-content-around">
                            <button className="btn btn-success">Add Car</button>
                            <Link to="../automobiles" className="btn btn-secondary w-25">Cancel</Link>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}
