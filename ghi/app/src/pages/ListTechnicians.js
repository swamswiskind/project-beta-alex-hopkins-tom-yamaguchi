import { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
import { DetailTable } from "../components/DetailTable";

export default function ListTechnicians() {
    const [details, setDetails] = useState([])
    const includedValues = ["employee_id", "first_name", "last_name"]
    const buttons = [{"name": "Fire", "class": "btn btn-danger", "click": handleDelete}]

    async function handleDelete(event) {
        const id = event.target.value
        const fetchConfig = {
            method: "delete",
            headers: {
                "Content-Type": 'application/json',
            }
        }
        const response = await fetch(`http://localhost:8080/api/technicians/${event.target.value}/`, fetchConfig)
        if (response.ok) {
            const copyData = [...details]
            setDetails(copyData.filter((detail) => detail.employee_id != id))
        }
    }

    const fetchData = async () => {
        const response = await fetch("http://localhost:8080/api/technicians/")
        if (response.ok) {
            const data = await response.json()
            setDetails(data.Technicians)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className="container">
            <div className="d-grid gap-2 d-sm-flex justify-content-between" style={{margin: "1rem"}}>
                <h3>Technician:</h3>
                <Link to="new" className="btn btn-success btn-lg px-4 gap-3">Hire a Technician</Link>
            </div>
            <hr/>
            {details.length > 0 ? DetailTable(details, includedValues, buttons, "employee_id") : null}
        </div>
    )
}