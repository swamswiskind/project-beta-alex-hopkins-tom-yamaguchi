import { useEffect, useState } from "react"


export function useDefaultState() {
    const [state, setState] = useState("")

    function handleChange(event) {
        event.preventDefault()
        setState(event.target.value)
    }

    function reset() {
        setState("")
    }

    return {"state": state, "handleChange": handleChange, "reset": reset}
}


export function useListState(){
    const [list, setList] = useState([])

    function initializeList(data) {
        setList(data)
    }

    return {"list": list, "setList": initializeList}
}