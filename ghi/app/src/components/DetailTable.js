import React from "react";


/**
 * Creates a table of specified values and optional buttons
 * @param {Array<{}>} data Array of Objects
 * @param {Array<String>} includedValues Array of strings, used in table headers and filtering data objects by matching key
 * @param {Array<{}>} buttons Array of Objects [{"name": btn name, "class": bootstrap btn class, "click": onClick function}, ...]
 * @param {String} selector key for unique property of data objects
 * @returns
 */
export function DetailTable(data, includedValues, buttons = null, selector = null) {
    function confirmModal(id, item) {
        return(
            <div className="modal fade" id={`${item.name}Model`} tabIndex="-1" aria-labelledby={`${item.name}ModelLabel`} aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id={`${item.name}ModelLabel`}>Confirm Action: {item.name}</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="button" value={id} onClick={item.click} data-bs-dismiss="modal" className="btn btn-primary">Confirm</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    return (
        <div className="shadow" >
            <table className="table table-success table-striped align-middle text-center" key="table">
                <thead >
                    <tr className="table-dark" key="head">
                        {includedValues.map((key) => <th key={key} scope="col">{key.toUpperCase().replaceAll("_", " ")}</th>)}
                        {buttons ? <th key={"actions"} scope="col">ACTIONS</th> : null}
                    </tr>
                </thead>
                <tbody>
                    {data.map((item, index) => {
                        return(
                            <tr key={`${index}${item}`}>
                                {includedValues.map(value => <td key={`${item[value]}${index}${Math.random() * 1000}`}>{item[value]}</td>)}
                                {buttons
                                    ?
                                    <td key={`${item[selector]}${index.toString()}`}>
                                        {buttons.map(button => {
                                            return(
                                                <React.Fragment key={button.name}>
                                                    <button
                                                        type="button"
                                                        data-bs-toggle="modal"
                                                        data-bs-target={`#${button.name}Model`}
                                                        className={button.class}
                                                        >
                                                        {button.name}
                                                    </button>
                                                    {confirmModal(item[selector], button)}
                                                </React.Fragment>
                                            )})}
                                    </td>
                                    :
                                    null
                                }
                            </tr>
                            )})}
                </tbody>
            </table>
        </div>
    )
}
