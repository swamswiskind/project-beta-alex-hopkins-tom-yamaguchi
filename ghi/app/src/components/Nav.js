import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" activeclassname="active" aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" activeclassname="active" aria-current="page" to="inventory/manufacturers">Manufacturers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" activeclassname="active" aria-current="page" to="inventory/models">Models</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" activeclassname="active" aria-current="page" to="inventory/automobiles">Automobiles</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" activeclassname="active" aria-current="page" to="sales-department/salespeople">Salespeople</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" activeclassname="active" aria-current="page" to="sales-department/customers">Customers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" activeclassname="active" aria-current="page" to="sales-department/sales">Sales</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" activeclassname="active" aria-current="page" to="sales-department/history">Sales History</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" activeclassname="active" aria-current="page" to="service-department/technicians">Technicians</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" activeclassname="active" aria-current="page" to="service-department/appointments">Service Appointments</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" activeclassname="active" aria-current="page" to="service-department/history">Service History</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
